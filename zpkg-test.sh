#!/bin/sh
set -o errexit
set -o nounset

./zpkg-clean.sh
python2.6 ./setup.py test
python2.7 ./setup.py test
python3 ./setup.py test
