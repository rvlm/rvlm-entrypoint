import sys
import pytest
import rvlm.entrypoint

if sys.version_info[0] == 3:
    from _test_entrypoint_3 import *


def test_1_python2and3():

    def target(command, verbose=False, level=0, *files):
        return (command, verbose, level, files)

    def call(*args, **kwargs):
        return rvlm.entrypoint.call(target, args, **kwargs)

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("--silent", "command")

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("command", "--verbose", "--level")

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("command", "--verbose", emptyVarArgs = False)

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("command", "-s")

    assert(call("command")              == ("command", False, 0, ()))
    assert(call("--verbose", "command") == ("command", True,  0, ()))
    assert(call("-v", "command")        == ("command", True,  0, ()))
    assert(call("command", "-v")        == ("command", True,  0, ()))
    assert(call("--verbose", "--level", "2", "command")
                                        == ("command", True,  2, ()))
    assert(call("-l", "2", "command")   == ("command", False, 2, ()))
    assert(call("-vl2", "command")      == ("command", True,  2, ()))
    assert(call("--verbose", "command", "--", "--level", "2")
                           == ("command", True, 0, ("--level", "2")))
    assert(call("--no-verbose", "command")
                                        == ("command", False, 0, ()))

def test_2_python2and3():

    def target(verbosityLevel        = 0,
               __something_very_Bad  = 0.2,
               OR_MAY_BE_EVEN__WORSE42 = "42"):
        return (verbosityLevel, __something_very_Bad, OR_MAY_BE_EVEN__WORSE42)

    def call(*args, **kwargs):
        return rvlm.entrypoint.call(target, args, **kwargs)

    assert(call("--verbosity-level",        "100500",
                 "--something-very-bad",     "99.1",
                 "--or-may-be-even-worse42", "even worse")
                                        == (100500, 99.1, "even worse"))
