import rvlm.entrypoint
import pytest

# ---
def test_1_python3():

    print(rvlm.entrypoint.__file__)

    def target(command, verbose=False, *files, level=0):
        return (command, verbose, level, files)

    def call(*args, **kwargs):
        return rvlm.entrypoint.call(target, args, **kwargs)

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("--silent", "command")

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("command", "--verbose", "--level")

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("command", "--verbose", emptyVarArgs = False)

    with pytest.raises(rvlm.entrypoint.ParserError):
        call("command", "-s")

    assert(call("command")              == ("command", False, 0, ()))
    assert(call("--verbose", "command") == ("command", True,  0, ()))
    assert(call("-v", "command")        == ("command", True,  0, ()))
    assert(call("command", "-v")        == ("command", True,  0, ()))
    assert(call("--verbose", "--level", "2", "command")
                                        == ("command", True,  2, ()))
    assert(call("-l", "2", "command")   == ("command", False, 2, ()))
    assert(call("-vl2", "command")      == ("command", True,  2, ()))
    assert(call("--verbose", "command", "--", "--level", "2")
                           == ("command", True, 0, ("--level", "2")))
    assert(call("--no-verbose", "command")
                                        == ("command", False, 0, ()))
