#!/bin/sh
set -o errexit
set -o nounset

./zpkg-test.sh
python ./setup.py build sdist register upload
