#!/bin/sh
set -o errexit
set -o nounset

./zpkg-clean.sh
sphinx-build -b html . _build/html
